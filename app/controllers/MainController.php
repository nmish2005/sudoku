<?php

namespace sudoku\controllers;

use sudoku\core\Controller;

class MainController extends Controller
{
    function action_404()
    {
        $this->View->generate("404.php");
    }

    function action_index()
    {
        $this->View->generate("main.php");
    }

    function action_test()
    {
        echo 'test';
    }
}