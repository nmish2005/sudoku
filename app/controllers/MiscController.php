<?php

namespace sudoku\controllers;


use sudoku\core\Controller;

class MiscController extends Controller
{

    function action_index(){
        $this->View->generate('misc/index.php');
    }

}