<?php

namespace sudoku\controllers;

use sudoku\core\App;
use sudoku\core\Controller;
use sudoku\models\Levels;


class PlayController extends Controller
{

    function action_index()
    {

        $this->View->generate("play/index.php");

    }

    function action_form()
    {

        $this->View->generate("play/form.php");

    }

    function action_sudoku()
    {

        App::addCss('game.css');

        $this->View->generate("play/sudoku.php", [

            'gameLevelData' => Levels::getLevelByNumByType($_GET['difficulty'], $_GET['num'])

        ]);

    }

    function action_fifteens()
    {

        App::addJs('play/fifteens.js');
        App::addCss('game.css');

        $this->View->generate("play/fifteens.php");

    }

    function action_horseMove()
    {

        App::addJs('play/horseMove.js');
        App::addCss('game.css');

        $this->View->generate("play/horseMove.php");

    }

}