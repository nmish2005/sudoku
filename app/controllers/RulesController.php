<?php

namespace sudoku\controllers;


use sudoku\core\Controller;
use sudoku\models\Rules;

class RulesController extends Controller
{

    function action_index(){
        $this->action_sudoku();
    }

    function action_sudoku(){

        $this->View->generate('rules/sudoku.php', [

            'rule' => Rules::getRuleByGame('sudoku')

        ]);

    }

    function action_fifteens(){

        $this->View->generate('rules/fifteens.php', [

            'rule' => Rules::getRuleByGame('fifteens')

        ]);

    }

    function action_horseMove(){

        $this->View->generate('rules/horseMove.php', [

            'rule' => Rules::getRuleByGame('horseMove')

        ]);

    }

}