<?php

namespace sudoku\core;


class App
{

    static function addJs($arr)
    {
        Settings::getInstance()->param(
            'js',
            array_merge(
                Settings::getInstance()->param('js'),
                (array) $arr ));
    }

    static function addCss($arr)
    {
        Settings::getInstance()->param(
            'css',
            array_merge(
                Settings::getInstance()->param('css'),
                (array) $arr ));
    }

    static function genJs()
    {
        $html = "";
        $js = Settings::getInstance()->param('js');
        foreach ($js as $item){
            $html .= '<script src="/assets/js/' . $item . '"></script>';
        }

        return $html;
    }

    static function genCss()
    {
        $html = "";
        $css = Settings::getInstance()->param('css');
        foreach ($css as $item){
            $html .= '<link href="/assets/css/' . $item . '" rel="stylesheet">';
        }

        return $html;
    }

    static function getDataFromSettings($name){
        return Settings::getInstance()->param($name);
    }

}