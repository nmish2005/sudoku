<?php

namespace sudoku\core;


class DBase
{

    private static $_instance = null;

    private function __construct()
    {

        include_once $_SERVER['DOCUMENT_ROOT'] . '/conf.php';

        self::$_instance = mysqli_connect($dbSettings['db_host'], $dbSettings['db_user'], $dbSettings['db_pass'], $dbSettings['db_name']) or die("DB ERROR");

    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance()
    {
        if (!is_null(self::$_instance)) {
            return self::$_instance;
        }

        new self;

        return self::$_instance;
    }
}