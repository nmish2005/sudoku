<?php
namespace sudoku\core;

class Router
{
    static function Launch()
    {
        $uriParts = array_values(array_filter(explode("/", $_SERVER['REQUEST_URI'])));

        // получаем имя контроллера и экшна
        $controller_name = empty($uriParts[0])? "main" : $uriParts[0];
        $action_name = empty($uriParts[1]) ? "index" : $uriParts[1];

        // добавляем префиксы
        $model_name = ucfirst($controller_name);
        $controller_name = '\\sudoku\\controllers\\' . ucfirst($controller_name) . 'Controller';
        $action_name = 'action_' . strtolower($action_name);

        // подцепляем файл с классом контроллера
        if(!class_exists($controller_name))
            return Router::ErrorPage404();

        // создаем контроллер
        $controller = new $controller_name;

        if (!method_exists($controller, $action_name))
            return Router::ErrorPage404(); // здесь также разумнее было бы кинуть исключение

        $controller->$action_name(); // вызываем действие контроллера

        return 'Router launched';
        }

    static function ErrorPage404() {

        http_response_code(404);

        $mainController = new \sudoku\controllers\MainController();
        $mainController->action_404();

        return 404;
    }
}