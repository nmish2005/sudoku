<?php

namespace sudoku\core;


class Settings
{
    private static $_instance = null;

    private $settings = [
        'projectName' => 'MySudoku',
        'siteUrl' => 'http://sudoku.in.ua',
        'js' => [
            '../libs/jq.js',
            '../libs/swal2/sweetalert2.min.js',
            '../libs/popper.js',
            '../libs/bs/js/bootstrap.min.js',
            'main.js'
        ],
        'css' => [
            'main.css',
            '../libs/fa/css/font-awesome.min.css',
            '../libs/animate.css',
            '../libs/swal2/sweetalert2.min.css',
            '../libs/bs/css/bootstrap.min.css'

        ]];

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance()
    {
        if (!is_null(self::$_instance)) {
            return self::$_instance;
        }

        self::$_instance = new self;

        return self::$_instance;
    }

    function param($name, $value = [])
    {
        if (empty($value))
            return isset($this->settings[$name])
                ? $this->settings[$name]
                : null;

        return $this->settings[$name] = $value;
    }
}