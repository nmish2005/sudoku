<?php
namespace sudoku\core;

class View
{
    function generate($content_view, $dataProvider = null, $template_view = "layout.php")
    {
        include $_SERVER['DOCUMENT_ROOT'] . '/app/views/' . $template_view;
    }
}