<?php

namespace sudoku\models;


use sudoku\core\DBase;

class Rules
{

    static function getRuleByGame($game = 'sudoku') {

        $link = DBase::getInstance();

        mysqli_query($link, 'SET NAMES utf8');

        $query = mysqli_query($link, "SELECT `content` FROM `rules` WHERE `game` = '{$game}'");

        $row = mysqli_fetch_assoc($query);

        return $row['content'];

    }

}