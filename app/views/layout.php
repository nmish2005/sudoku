<?
namespace sudoku\views;

use sudoku\core\App;

?>
<!DOCTYPE html>
<html>
<head>

    <title>Судоку</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="utf-8">
    <!-- Icon -->
    <link rel="icon" type="icon" href="/assets/img/icon.ico">
    <!-- CSS -->
    <?= App::genCss() ?>
    <!-- JS -->
    <?= App::genJs() ?>

</head>
<body onselectstart="return false">
<nav class="navbar navbar-expand-lg navbar-light bg-light animated">
    <a class="navbar-brand disable_animation" href="/">
        <img src="/assets/img/icon.ico" width="30" height="30" class="d-inline-block align-top" alt="Home">
        MySudoku
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item" id="main">
                <a class="nav-link disable_animation" href="/">Домашняя страница</a>
            </li>
            <li class="nav-item dropdown" id="rules">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Правила игры
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item disable_animation" href="/rules/sudoku">Судоку</a>
                    <a class="dropdown-item disable_animation" href="/rules/fifteens">Пятнашки</a>
                    <a class="dropdown-item disable_animation" href="/rules/horseMove">Ход конём</a>
                </div>
            </li>
            <li class="nav-item" id="play">
                <a class="nav-link disable_animation" href="/play">Играть</a>
            </li>
        </ul>
    </div>
</nav>
<!-- end navbar -->

<? include $_SERVER['DOCUMENT_ROOT'] . '/app/views/' . $content_view; ?>

<!-- start footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-4">
                <div>
                    <h1 class="logo">My<span class="logoSpan logo">Sudoku</span></h1>
                </div>
                <br><br>
                <hr>
                <small>MySudoku &copy; <?= date('Y') ?></small>
            </div>
            <div class="col-4">
                <span class="fa-stack fa-lg">
  <i class="fa fa-circle fa-stack-2x"></i>
  <i class="fa fa-map-marker fa-stack-1x fa-inverse"></i>
</span>
                <a href="https://goo.gl/maps/vVK2qEKzxJz" class="text">
                    Хмельницкое шоссе, 41<br><b>Винница, Украина</b>
                </a>
                <br><br>
                <span class="fa-stack fa-lg">
  <i class="fa fa-circle fa-stack-2x"></i>
  <i class="fa fa-phone fa-stack-1x fa-inverse"></i>
</span>
                <b><a href="tel:+79225951564" class="text">+7 (922) 595-1564</a></b>
                <br><br>
                <span class="fa-stack fa-lg">
  <i class="fa fa-circle fa-stack-2x"></i>
  <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
</span>
                <a href="mailto:admin@sudoku.in.ua" class="text">
                    admin@sudoku.in.ua
                </a>
            </div>
            <div class="col-4">
                <h3>
                    О проекте
                </h3>
                <br>
                Привет! Это мой проект Судоку. Весь мой код открыт для мира. Вы можете увидеть этот проект на сайте
                GitLab.
                Все эти игры сделаны на Javascript и JS-библиотеке jQuery. Я также использую PHP в качестве серверной
                части (бэкэнда).
                <br><br>
                <a href="https://vk.com/nmish2005"><span class="fa-stack fa-lg">
  <i class="fa fa-square fa-stack-2x"></i>
  <i class="fa fa-vk fa-stack-1x fa-inverse"></i>
</span></a>
                <a href="https://instagram.com/nmish2005"><span class="fa-stack fa-lg">
  <i class="fa fa-square fa-stack-2x"></i>
  <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
</span></a>
                <a href="https://skype.com"><span class="fa-stack fa-lg">
  <i class="fa fa-square fa-stack-2x"></i>
  <i class="fa fa-skype fa-stack-1x fa-inverse"></i>
</span></a>
                <a href="https://gitlab.com/nmish2005/sudoku"><span class="fa-stack fa-lg">
  <i class="fa fa-square fa-stack-2x"></i>
  <i class="fa fa-gitlab fa-stack-1x fa-inverse"></i>
</span></a>
            </div>
        </div>
    </div>
</footer>
<!-- end footer -->
</body>
</html>
