<?php
namespace sudoku\views;
?>
<div id="home">
    <div id="preview">

        <h1 style="color: ghostwhite !important;">Судоку</h1>
        <br>
        <div class="row">

            <div class="col-2"></div>

            <div class="col-8">

                Одна из самых популярных игр, посвященных любителям головоломок. Этот сайт поможет Вам учиться и
                улучшить ваши навыки игры в судоку. Размещенные тут игры непременно Вам понравятся.

            </div>

            <div class="col-2"></div>

        </div>
        <br>
        <a href="#sudoku_link">
            <button class="btn btn-outline-light">Вперёд!</button>
        </a>
    </div>
</div>

<br>

<div id="sudoku_link" style="height: 100px;"></div>

<h2>Судоку</h2>

<br>

<div id="sudoku">

    <div class="row">

        <div class="col-8"></div>

        <div class="col-4">

            <h3>Про игру</h3>
            Судо́ку — головоломка с числами. Судоку активно публикуют газеты и журналы разных стран мира, сборники
            судоку издаются большими тиражами. Решение судоку — популярный вид досуга.
            <br>
            <a href="/rules/sudoku">Больше</a>
            <br><br>
            <a href="/play/sudoku">
                <button class="btn btn-primary">Играть</button>
            </a>

        </div>

    </div>

</div>

<br>

<h2>Пятнашки</h2>

<br>

<div id="fifteens">

    <div class="row">

        <div class="col-8"></div>

        <div class="col-4">

            <h3>Про игру</h3>
            Игра в 15, или пятнашки — популярная головоломка, придуманная в 1878 году Ноем Чепмэном. Представляет собой
            набор одинаковых квадратных костяшек с нанесёнными числами от 1 до 15, заключённых в квадратную коробку.
            <br>
            <a href="/rules/fifteens">Больше</a>
            <br><br>
            <a href="/play/fifteens">
                <button class="btn btn-primary">Играть</button>
            </a>

        </div>

    </div>

</div>


<br>

<h2>Ход конём
    <small><sup><span class="badge badge-success">New</span></sup></small>
</h2>

<br>

<div id="horseMove">

    <div class="row">

        <div class="col-8"></div>

        <div class="col-4">

            <h3>Про игру</h3>
            Ход конём, или Задача о ходе коня — игра, суть которой найти маршрут для шахматного коня, проходя через все
            поля доски 10 на 10 только по одному разу. Эта задача известна ещё с XVIII века.
            <br>
            <a href="/rules/horseMove">Больше</a>
            <br><br>
            <a href="/play/horseMove">
                <button class="btn btn-primary">Играть</button>
            </a>

        </div>

    </div>

</div>