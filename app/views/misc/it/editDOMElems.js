document.write("<script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E=\" crossorigin=\"anonymous\"></script>");

$(function () {
    alert('editDOMElems ready to edit!');
    $('*').click(function () {
        $('#active_elem').attr('id', '');
        $(this).attr('id', 'active_elem');
    });
});

$('#active_elem').keypress(function (e) {
    if ((e.ctrlKey && e.keyCode == 'E'.charCodeAt(0))) {
        edit($(this));
        return false;
    }
});

function edit(elem) {
    elem.attr('contenteditable', 'contenteditable');
}
