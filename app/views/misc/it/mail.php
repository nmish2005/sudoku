<?php

error_reporting(E_ALL);

ini_set("display_errors", "1");

require_once $_SERVER["DOCUMENT_ROOT"] . "/vendor/autoload.php";

$mailer = new PHPMailer;

$mailer->From = "from@yourdomain.com";
$mailer->FromName = "Full Name";

$mailer->addAddress("nmish2005@gmail.com");

$mailer->isHTML(true);

$mailer->Subject = "Subject Text";
$mailer->Body = "<i>Mail body in HTML</i>";
$mailer->AltBody = "This is the plain text version of the email content";

if(!$mailer->send())
{
    echo "Mailer Error: " . $mailer->ErrorInfo;
}
else
{
    echo "Message has been sent successfully";
}