<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>PHP Post and Get</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>
<body>
<div class="container"><form method="get" action="postget.php">
    <input type="text" placeholder="Some text here..." name="get_info">
    <input type="submit" value="GET" class="btn btn-outline-info">
</form>
<br>
<form method="post" action="postget.php">
    <input type="text" placeholder="Some text here..." name="post_info">
    <input type="submit" class="btn btn-outline-primary" value="POST">
</form>
<br>
<a href="postget.php"><button type="button" class="btn btn-light">Reload</button></a>
<br><br>
<div class="alert alert-success" role="alert">
<?php
$get = $_GET['get_info'];
$post = $_POST['post_info'];
if($post == null){
    echo $get;
}   else    {
    echo $post;
}
?>
</div>
</div>
</body>
</html>