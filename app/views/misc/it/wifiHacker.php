<?php

function generatePasswd($length = 8) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function connect($ssid, $passwd){
    $connect = system("nmcli dev wifi connect {$ssid} password {$passwd}");
    return $connect;
}

$length = readline("Количество символов в пароле (оставить поле пустым - стандартное значение - 8.) \n \n \n");
$ssid = readline("Какой Wi-Fi будем взламывать? \n \n \n");

if($length == ''){
    $passwd = generatePasswd();
}   else    {
    $passwd = generatePasswd($length);
}
$result = connect( $ssid, $passwd );
echo $result;