<?php

namespace sudoku\views\play;

?>

<script>
    if (getCookie('fifteensProgress') == undefined)
        setCookie('fifteensProgress', '', {expires: 94672800, path: '/'});
</script>

<div class="container">
    <div id='header'></div>
    <br>
    <div id="gameZoomer">
        <button class='button1' style='border-radius: 50%; height: 40px' onclick='updateZoom(0.1)'>+</button>
        <br>
        <i class="fa fa-search fa-2x"></i>
        <br>
        <button class='button1' style='border-radius: 50%; height: 40px' onclick='updateZoom(-0.1)'>-</button>

    </div>
    <div id='game'></div>
    <br>
    <div id='buttons'></div>
</div>

<br>
<br>
<br>