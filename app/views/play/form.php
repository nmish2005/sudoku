<?php

namespace sudoku\views\play;

?>
<div class="container" id="form">
    <form method='get' action="/play/sudoku/">
        <div class="row">
            <div class="col-3"></div>
            <div class="col-6">
                <div class="form-group">
                    <label for="exampleFormControlSelect2">Выберите сложность</label>
                    <br>
                    <select name="difficulty" class="custom-select" required id="exampleFormControlSelect2">
                        <option value="easy">Легкий</option>
                        <option value="medium">Средний</option>
                        <option value="hard">Сложный</option>
                    </select>
                </div>
                <br>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Выберите уровень</label>
                    <br>
                    <select class="custom-select" required id="exampleFormControlSelect1" name="num">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                    </select>
                </div>
                <br><br>
                <button type="submit" class="btn btn-primary mb-2">Играть</button>
            </div>
            <div class="col-3"></div>
        </div>
    </form>
</div>

<br><br><br><br><br><br><br><br><br><br>