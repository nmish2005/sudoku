<?php

namespace sudoku\views\play;

?>
<div class="container">
<div class='row'>
    <div class='col-4'>
        <div class='list-group' id='list-tab' role='tablist'>
            <a class='list-group-item list-group-item-action active' id='list-sudoku-list' data-toggle='list'
               href='#list-sudoku' role='tab' aria-controls='sudoku'>Судоку</a>
            <a class='list-group-item list-group-item-action' id='list-fifteens-list' data-toggle='list'
               href='#list-fifteens' role='tab' aria-controls='fifteens'>Пятнашки</a>
            <a class='list-group-item list-group-item-action' id='list-horseMove-list' data-toggle='list'
               href='#list-horseMove' role='tab' aria-controls='horseMove'>Ход конём</a>
        </div>
    </div>
    <div class='col-8'>
        <div class='tab-content' id='nav-tabContent'>
            <div class='tab-pane fade show active' id='list-sudoku' role='tabpanel' aria-labelledby='list-sudoku-list'>
                Судо́ку — головоломка с числами. Судоку активно публикуют газеты и журналы разных стран мира, сборники
                судоку издаются большими тиражами. Решение судоку — популярный вид досуга.
                <br><a href='/play/sudoku'>
                    <button class='btn btn-outline-success'>Вперёд!</button>
                </a></div>
            <div class='tab-pane fade' id='list-fifteens' role='tabpanel' aria-labelledby='list-fifteens-list'>Игра в
                15, или пятнашки — популярная головоломка, придуманная в 1878 году Ноем Чепмэном. Представляет собой
                набор одинаковых квадратных костяшек с нанесёнными числами от 1 до 15, заключённых в квадратную коробку.
                <br><a href='/play/fifteens'>
                    <button class='btn btn-outline-success'>Вперёд!</button>
                </a></div>
            <div class='tab-pane fade' id='list-horseMove' role='tabpanel' aria-labelledby='list-horseMove-list'>Ход
                конём, или Задача о ходе коня — игра, суть которой найти маршрут для шахматного коня, проходя через все
                поля доски 10 на 10 только по одному разу. Эта задача известна ещё с XVIII века.
                <br><a href='/play/horseMove'>
                    <button class='btn btn-outline-success'>Вперёд!</button>
                </a></div>
        </div>
    </div>
</div>
</div>

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>