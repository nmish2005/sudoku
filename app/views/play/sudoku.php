<?php

namespace sudoku\views\play;

?>
<div class="container">
    <script>
        if (getCookie('sudokuProgress') == undefined)
            setCookie('sudokuProgress', '', {expires: 94672800, path: '/'});

        var gld = null, num = parseInt("<?=$_GET['num']?>") || null,
            difficulty = "<?=$_GET['difficulty']?>" || null;

        if (!$.isEmptyObject(getCookie('sudokuProgress'))) {
            if (JSON.parse(getCookie('sudokuProgress')).num != num)
                if (JSON.parse(getCookie('sudokuProgress')).difficulty != difficulty)
                    location = '/play/sudoku/?difficulty=' + JSON.parse(getCookie('sudokuProgress')).difficulty + '&num=' + JSON.parse(getCookie('sudokuProgress')).num;
        } else if (num == null || difficulty == null)
            location = '/play/form';

        gld = JSON.parse('<?=$dataProvider["gameLevelData"]?>');
    </script>
    <script src="/assets/js/play/sudoku.js"></script>

    <div id='header'></div>
    <br>
    <div id="gameZoomer">
        <button class='button1' style='border-radius: 50%; height: 40px' onclick='updateZoom(0.1)'>+</button>
        <br>
        <i class="fa fa-search fa-2x"></i>
        <br>
        <button class='button1' style='border-radius: 50%; height: 40px' onclick='updateZoom(-0.1)'>-</button>

    </div>
    <div id='game'></div>
    <br>

    <div class="keyboard">
        <button class="button2" num="1"></button>
        <button class="button2" num="2"></button>
        <button class="button2" num="3"></button>
        <button class="button2" num="4"></button>
        <button class="button2" num="5"></button>
        <button class="button2" num="6"></button>
        <button class="button2" num="7"></button>
        <button class="button2" num="8"></button>
        <button class="button2" num="9"></button>
    </div>

    <br>
    
    <div id='buttons'></div>

    <br>
    <br>
</div>
