"use strict";

var permission = null, moreThanHome, zoomLevel = 1;

Notification.requestPermission(function (result) {
    permission = result;
    return result;
});

$(function () {
    addActive();
    $("a[href^='#']").click(function (event) {
        if ($(this).attr('href') != '#') {

            //отменяем стандартную обработку нажатия по ссылке
            event.preventDefault();

            //забираем идентификатор бока с атрибута href
            var id = $(this).attr('href'),

                //узнаем высоту от начала страницы до блока на который ссылается якорь
                top = $(id).offset().top;

            //анимируем переход на расстояние - top за 2с
            $('body,html').animate({scrollTop: top}, 2000);
        }
    });

    if (window.location.pathname == '/'
        || window.location.pathname == '/main'

        || window.location.pathname == '/main/index'
    ) {

        var homeHeight = $("#home").height();

        $(window).scroll(function () {

            var scroll = $(this).scrollTop();
            if (scroll > homeHeight) {

                if ($("nav").hasClass("fadeOutUp")) {

                    $("nav").removeClass("fadeOutUp");

                }
                $("nav").css("display", "block");
                $("nav").addClass("fadeInDown");
                moreThanHome = true;

            } else if (moreThanHome == true) {

                if ($("nav").css("display") == "block") {

                    if ($("nav").hasClass("fadeInDown")) {

                        $("nav").removeClass("fadeInDown");

                    }

                    $("nav").addClass("fadeOutUp");
                    setTimeout(function () {
                        $("nav").css("display", "none");
                    }, 1000);

                }

                moreThanHome = false;

            }

        });
    } else {
        $("nav").css('display', 'block');
        $("body").css('margin-top', '120px');
    }

});

function updateZoom(zoom) {
    zoomLevel = zoomLevel || 1;
    zoomLevel += zoom;
    $('body').css({ zoom: zoomLevel, '-moz-transform': 'scale(' + zoomLevel + ')' });
    $('body').css({ zoom: zoomLevel, '-webkit-transform': 'scale(' + zoomLevel + ')' });
    $('body').css({ zoom: zoomLevel, '-o-transform': 'scale(' + zoomLevel + ')' });
    $('body').css({ zoom: zoomLevel, 'transform': 'scale(' + zoomLevel + ')' });

    return zoomLevel;
}

function getGet(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
            tmp = item.split("=");
            if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}

function addActive() {
    var elem;

    switch (window.location.pathname.toString().split("/")[1]) {
        case 'play':
            elem = $("#play");
            break;

        case 'rules':
            elem = $("#rules");
            break;

        case '404':
            elem = $(".navbar-brand");
            break;


        default:
            elem = $("#main");
    }

    elem.addClass('active');

    return elem
}

function emptyFunction() {
}

function Interceptor(preInvoke, postInvoke) {
    /*
     * Если preInvoke не функция, то заменяем этот аргумент
     * на пустую функцию. Вдруг нам не нужно будет ничего делать
     * до перехватываемого вызова.
     */
    var preInvoke = typeof preInvoke === 'function' ?
        preInvoke : emptyFunction,

        // Аналогично с postInvoke
        postInvoke = typeof postInvoke === 'function' ?
            postInvoke : emptyFunction;

    this.preInvoke = preInvoke;
    this.postInvoke = postInvoke;
}

Interceptor.prototype = {
    constructor: Interceptor,
    interceptInvokes: function (callback) {
        /*
         * Запоминаем текущий контекст, так как в
         * в следующей анонимной функции, но уже другой
         */
        var self = this;
        return function () { // преобразованная функция
            // конвертируем arguments в массив
            var args = Array.prototype.slice.call(arguments, 0),
                /*
                 * Массив с аргументами для preInvoke и postInvoke.
                 * Добавим в него в качестве первого элемента функцию,
                 * вызов которой перехватывается. Вдруг нам понадобится дополнительная информация о ней.
                 */
                result;

            // Делаем что-то до перехватываемого вызова
            self.preInvoke.call(self, callback, args);
            result = callback.apply(self, args);
            // Делаем что-то после
            self.postInvoke.call(self, callback, args, result);

            return result;
        };
    }
};

// Fn Interceptor

function printCallInfo(callback, args) {
    var logStr;
    args == null ? logStr = "Invoking function with no args"
        :
        logStr = "Invoking function '" + callback.name + "' with args: " + args.join(', ');

    console.log(logStr);
}

function printCallResult(callback, args, result) {
    result == undefined ?
        console.warn("Function '" + callback.name + "' returned undefined, it may contain errors!")
        :
        console.log("Function '" + callback.name
            + "' is successfully invoke\nresult: " + result + "\n");
}

var logInterceptor = new Interceptor(printCallInfo, printCallResult);

getGet = logInterceptor.interceptInvokes(getGet);
addActive = logInterceptor.interceptInvokes(addActive);
emptyFunction = logInterceptor.interceptInvokes(emptyFunction);
sendNotification = logInterceptor.interceptInvokes(sendNotification);
destroyCookie = logInterceptor.interceptInvokes(destroyCookie);
redirect2Game = logInterceptor.interceptInvokes(redirect2Game);
getCookie = logInterceptor.interceptInvokes(getCookie);
setCookie = logInterceptor.interceptInvokes(setCookie);
updateZoom = logInterceptor.interceptInvokes(updateZoom);

//Notification sender

function redirect2Game() {

    location = "/play/";
    return location;

}

function sendNotification(title, options) {

    var notification = new Notification(title, options);

    notification.onclick = redirect2Game;

    return "Notification sended";

}

if (permission == "granted") {

    setTimeout(function () {

        sendNotification("Не забудьте поиграть!",
            {

                icon: "/assets/img/icon.ico",
                lang: "ru",
                dir: "ltr"

            });

        setInterval(function () {

            sendNotification("Не забудьте поиграть!",
                {

                    icon: "/assets/img/icon.ico",
                    lang: "ru",
                    dir: "ltr"

                })
        }, 300000)
    }, 300000);

}

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options) {
    options = options || {
        expires: 3600,
        path: '/'
    };

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;

    return document.cookie;
}

function destroyCookie(name) {
    setCookie(name, "", {
        expires: 0
    });

    return getCookie(name);
}