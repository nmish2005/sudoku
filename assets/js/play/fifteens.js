var game = [[1, 5, 9, 13], [2, 6, 10, 14], [3, 7, 11, 15], [4, 8, 12, 0]];

function echoTable() {

    var table = "<table class='fifteens_game' id='bambooked' align='center'>";

    for (let i = 0; i < 4; i++) {

        table += "<tr>";

        for (let j = 0; j < 4; j++)
            table += "<td onclick='cellClick(" + j + ", " + i + ");' posy='" + i + "' posx='" + j + "'></td>";

        table += "</tr>"

    }

    table += "</table>";

    $("#game").html(table);
    $("#header").html("<h1>Пятнашки</h1>");
    $("#buttons").html("<button class='button1' onclick='restartTheGame()'>Перемешать</button>");

    return "table created";

}

$(function () {

    initGame();
    echoTable();
    fillTable();

    if (getCookie('fifteensProgress') != '') {
        game = JSON.parse(getCookie('fifteensProgress'));
        fillTable();
        swal({
            title: 'Похоже, Вы не доиграли',
            type: 'info',
            confirmButtonColor: '#3BD80D',
            showCloseButton: false,
            showCancelButton: false,
            focusConfirm: false,
            text: "Игра восстановлена",
            confirmButtonText: "Отлично!",
            allowOutsideClick: false,
            allowEscapeKey: false
        });
    }

});

function cellClick(posX, posY) {

    var zeroX, zeroY;

    if (!isMoveValid(posX, posY))
        return false;

    for (let i = 0; i < 4; i++) {

        for (let j = 0; j < 4; j++) {

            if (game[i][j] == 0) {

                zeroX = i;
                zeroY = j;

            }

        }

    }


    swapGameElems(posX, posY, zeroX, zeroY);
    fillTable();
    gameOver();
    setCookie('fifteensProgress', JSON.stringify(game), {
        expires: 94672800,
        path: '/'
    });

    return "event proccessed";

}

function restartTheGame() {

    initGame();
    echoTable();
    fillTable();

    return "game restarted";

}

function randomInteger(min, max) {

    return Math.floor(Math.random() * (max - min + 1)) + min;

}

Array.prototype.swap = function (x, y, x1, y1) {

    var a = this[x][y];
    this[x][y] = this[x1][y1];
    this[x1][y1] = a;

    return this;

};

function initGame() {

    var x, y, x1, y1;

    for (let i = 0; i < 20; i++) {

        x = randomInteger(0, 3);
        y = randomInteger(0, 3);
        x1 = randomInteger(0, 3);
        y1 = randomInteger(0, 3);

        game.swap(x, y, x1, y1);

    }

    return game;

}

function gameOver() {

    if (JSON.stringify(game) == JSON.stringify([[1, 5, 9, 13], [2, 6, 10, 14], [3, 7, 11, 15], [4, 8, 12, 0]])) {

        setCookie('fifteensProgress', '', {expires: 94672800, path: '/'});

        swal({

            title: 'Вы выиграли!',
            type: 'success',
            confirmButtonColor: '#1e90ff',
            showCloseButton: false,
            focusConfirm: false,
            confirmButtonText: "Перемешать",
            showCancelButton: false,
            allowOutsideClick: false,
            allowEscapeKey: false
        }).then((result) => {
            if (result.value) {
                restartTheGame();
            }
        });

        return true;

    }
    else {

        return false;

    }

}

function isMoveValid(posX, posY) {

    return game[posX][posY - 1] != undefined && game[posX][posY - 1] == 0
        ||
        game[posX - 1] != undefined && game[posX - 1][posY] == 0
        ||
        game[posX][posY + 1] != undefined && game[posX][posY + 1] == 0
        ||
        game[posX + 1] != undefined && game[posX + 1][posY] == 0

}

function fillTable() {
    for (let i = 0; i < 4; i++) {
        for (let j = 0; j < 4; j++) {
            if (game[i][j] == 0)
                $("td[posx='" + i + "'][posy='" + j + "']").text('');
            else
                $("td[posx='" + i + "'][posy='" + j + "']").text(game[i][j]);
        }
    }
    return "table filled";
}

function swapGameElems(posX, posY, zeroX, zeroY) {
    game.swap(posX, posY, zeroX, zeroY);
    return "elems swapped";
}