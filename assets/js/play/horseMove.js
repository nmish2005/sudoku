"use strict";

var game = {};

function initGame() {

    for (var i = 0; i < 10; i++) {

        game[i] = [];

        for (var j = 0; j < 10; j++) {

            game[i][j] = 0;

        }

    }

    return "game array filled";

}

function echoTable() {

    var table = "<table id='bambooked' class='horseMove_game'>";

    for (var i = 0; i < 10; i++) {
        table += "<tr>";

        for (var j = 0; j < 10; j++) {

            table += "<td onclick='cellClick(" + j + ", " + i + ", this);' posY='" + i + "' posX='" + "" + j + "'></td>";

        }

        table += "</tr>"

    }

    table += "</table>";

    $("#game").html(table);
    $("#header").html("<h1>Ход конём</h1>");
    $("#buttons").html("<button class='button1' onclick='restartTheGame()'>Заново</button>");

    return "table created";

}

$(function () {

    initGame();
    echoTable();

    if (getCookie('horseMoveProgress') != undefined) {
        game = JSON.parse(getCookie('horseMoveProgress'));
        fillTable();
        lastElem();
        swal({
            title: 'Похоже, вы не доиграли',
            type: 'info',
            confirmButtonColor: '#3BD80D',
            text: "Игра восстановлена",
            showCloseButton: false,
            showCancelButton: false,
            focusConfirm: false,
            confirmButtonText: "Отлично!",
            allowOutsideClick: false,
            allowEscapeKey: false
        })
    }

});

function cellClick(posX, posY, element) {

    if (!isMoveValid(posX, posY))
        return false;

    var pos = posX + '' + posY;

    $("td").removeClass("active");

    if (element.className != "fixed")
        element.className = "active";
    else
        return false;

    moveTheHorse(posX, posY);
    gameOver();

    setCookie('horseMoveProgress', JSON.stringify(game), {
        expires: 94672800,
        path: '/'
    });

    return "event proccessed";

}

function moveTheHorse(posX, posY) {

    game[posX][posY] = getBiggestGameValue() + 1;

    fillTable();
    lastElem();

    return "horse moved";

}

function lastElem() {
    $("table tr td").removeClass('active');
    $("td:contains(" + getBiggestGameValue() + ")").addClass('active');
}

function fillTable() {
    for (var i = 0; i < 10; i++) {
        for (var j = 0; j < 10; j++) {
            if (game[i][j] == 0)
                $("td[posx='" + i + "'][posy='" + j + "']").text('');
            else
                $("td[posx='" + i + "'][posy='" + j + "']").text(game[i][j]).addClass('fixed');
        }
    }
}

function gameOver() {

    if (getBiggestGameValue() > 99) {

        setCookie('horseMoveProgress', '', {expires: 94672800, path: '/'});

        swal({

            title: 'Вы выиграли!',
            type: 'success',
            confirmButtonColor: '#1e90ff',
            showCloseButton: false,
            showCancelButton: false,
            focusConfirm: false,
            confirmButtonText: "Заново",
            allowOutsideClick: false,
            allowEscapeKey: false

        }).then((result) => {
            if (result.value) {
                restartTheGame()
            }
        });

        return true;

    }
    else {

        return false;

    }

}

function getBiggestGameValue() {

    var biggest = 0;

    for (var i = 0; i < 10; i++) {

        for (var j = 0; j < 10; j++) {

            if (game[i][j] > biggest)
                biggest = game[i][j];

        }

    }

    return biggest;

}

function getBiggestValuePosX() {

    var biggestValue = getBiggestGameValue();

    for (var i = 0; i < 10; i++) {

        for (var j = 0; j < 10; j++) {

            if (game[i][j] == biggestValue)
                return i;

        }

    }

}

function getBiggestValuePosY() {

    var biggestValue = getBiggestGameValue();

    for (var i = 0; i < 10; i++) {

        for (var j = 0; j < 10; j++) {

            if (game[i][j] == biggestValue)
                return j;

        }

    }

}

function getPosByValue(n) {

    return $("td:contains(" + n + ")").attr("id");

}

function isMoveValid(posX, posY) {

    var biggestValue = getBiggestGameValue();

    if (biggestValue == 0)
        return true;

    var lastX = getBiggestValuePosX(), lastY = getBiggestValuePosY(), deltaX = parseInt(Math.abs(posX - lastX)),
        deltaY = parseInt(Math.abs(posY - lastY));

    return (deltaX == 1 && deltaY == 2) || (deltaX == 2 && deltaY == 1);

}

function restartTheGame() {

    initGame();
    echoTable();

    return "game restarted";

}