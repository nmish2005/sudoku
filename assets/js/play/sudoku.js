"use strict";

var game = {};

function echoTable() {

    var table = "<table id='bambooked' class='sudoku_game' align='center'>";

    for (let i = 0; i < 9; i++) {

        table += "<tr>";

        for (let j = 0; j < 9; j++) {

            table += "<td id='" + j + "" + i + "' onclick='cellClick(" + j + ", " + i + ");' posY='" + i + "' posX='"
                + "" + j + "'></td>";

        }

        table += "</tr>"

    }

    table += "</table>";

    $("#game").html(table);
    $("#header").html("<h1>Судоку</h1>");
    $("#buttons").html("<button class='button1' onclick='restartTheGame()'>Заново</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class='button1' onclick='backToLvlSelect()'>Назад к выбору уровня</button>");

    return table;

}

function backToLvlSelect() {
    setCookie('sudokuProgress', '', {expires: 94672800, path: '/'});
    location = '/play/form';
}

function initGame() {

    for (let i = 0; i < 9; i++) {

        game[i] = [];

        for (let j = 0; j < 9; j++) {

            game[i][j] = 0;

        }
    }

    return game;

}

function fillTableFromTemplate() {

    for (let key in gld) {

        game[key[0]][key[1]] = gld[key];

    }

    fillTable('fromTpl');
}

function fillTable(mode = 'default') {

    switch (mode) {
        case 'fromTpl':
            for (let i = 0; i < 9; i++) {
                for (let j = 0; j < 9; j++) {
                    if (game[i][j] == 0)
                        $("td[posx='" + i + "'][posy='" + j + "']").text('');
                    else
                        $("td[posx='" + i + "'][posy='" + j + "']").text(game[i][j]).addClass('fixed');
                }
            }

            break;

        case 'fromCookie':

            for (let i = 0; i < 9; i++) {
                for (let j = 0; j < 9; j++) {
                    if (!$("td[posx='" + i + "'][posy='" + j + "']").hasClass('fixed'))
                        if(game[i][j] != 0)
                            $("td[posx='" + i + "'][posy='" + j + "']").text(game[i][j]);
                }
            }
            break;

        case 'default':
            for (let i = 0; i < 9; i++) {
                for (let j = 0; j < 9; j++) {
                    if (game[i][j] == 0)
                        $("td[posx='" + i + "'][posy='" + j + "']").text('');
                    else
                        $("td[posx='" + i + "'][posy='" + j + "']").text(game[i][j]);
                }
            }

            break;

        default:
            return console.error("Unknown mode specified for fillTable: " + mode);
    }


    return "table filled";

}

$(function () {

    echoTable();
    initGame();
    fillTableFromTemplate();
    gameOver();


    if (getCookie('sudokuProgress') != '') {
        swal({
            title: 'Похоже, Вы не доиграли',
            type: 'info',
            confirmButtonColor: '#3bd80d',
            text: "Игра восстановлена. Уровень восстановленной игры: " + translateDifficulty(difficulty) + ", " + num,
            showCloseButton: false,
            showCancelButton: false,
            focusConfirm: false,
            confirmButtonText: "Отлично!",
            allowOutsideClick: false,
            allowEscapeKey: false
        });

        var cookieGame = JSON.parse(JSON.parse(getCookie('sudokuProgress')).game);

        for (let i = 0; i < 9; i++) {
            for (let j = 0; j < 9; j++) {
                if (game[i][j] == 0) {
                    game[i][j] = cookieGame[i][j];
                }
            }
        }

        fillTable();
    }
    $("button[num]").click(
        function () {

            var activeField = $(".sudoku_game td.active");
            if (activeField.length != 0) {

                game[activeField.attr('posx')][activeField.attr('posy')] = parseInt($(this).html()) || 0;
                $(this).html('');

            }

            var posX = activeField.attr('posx'), posY = activeField.attr('posy'), usedNumbers = getSquareNums(posX, posY).concat(getYLineNums(posX), getXLineNums(posY));

            gameOver();
            fillTable();

            setCookie('sudokuProgress', JSON.stringify({
                game: JSON.stringify(game),
                num: num,
                difficulty: difficulty
            }), {
                expires: 94672800,
                path: '/'
            });

            keyboardFill(usedNumbers);
        });

});

function cellClick(posX, posY) {

    if($("tr td[posx='" + posX + "'][posy='" + posY + "']").hasClass('fixed'))
        return false;

    var activeField = $(".sudoku_game td.active"), num = activeField.attr('posx') + '' + activeField.attr('posy'),
        usedNumbers = getSquareNums(posX, posY).concat(getYLineNums(posX), getXLineNums(posY));

    keyboardFill(usedNumbers);

    $(".sudoku_game td").removeClass("active");

    var cell = document.getElementById(posX + "" + posY);

    if (cell.className != "fixed") {

        cell.className = "active";

    }

    return usedNumbers;

}

function translateDifficulty(difficulty = 'easy') {
    switch (difficulty) {
        case 'easy':
            return 'Лёгкий';
        case 'medium':
            return 'Средний';
        case 'hard':
            return 'Сложный';
        default:
            return console.error('Cannot translate difficulty: ' + difficulty);
    }
}

function squareSelect(posX, posY) {

    return Math.floor(posX / 3) + "" + Math.floor(posY / 3);

}

function getXLineNums(posY) {

    var arr1 = [];

    for (let i = 0; i < 9; i++) {

        arr1.push(game[i][posY]);

    }

    return arr1;

}

function getYLineNums(posX) {

    var arr2 = [];

    for (let i = 0; i < 9; i++) {

        arr2.push(game[posX][i]);

    }

    return arr2;

}

function getSquareNums(posX, posY) {

    var square = squareSelect(posX, posY), arr3 = [];

    for (let i = 0; i < 3; i++) {

        for (let j = 0; j < 3; j++) {

            arr3.push(game[square[0] * 3 + i][square[1] * 3 + j]);

        }

    }

    return arr3;

}

function gameOver() {

    var count = 0;

    for (let i = 0; i < 9; i++) {

        for (let j = 0; j < 9; j++) {

            if (game[i][j] != 0) {

                count += 1;

            }

        }

    }

    if (count == 81) {

        setCookie('sudokuProgress', '', {expires: 94672800, path: '/'});

        swal({

            title: 'Вы прошли этот уровень!',
            type: 'success',
            confirmButtonColor: '#1e90ff',
            showCloseButton: false,
            showCancelButton: false,
            focusConfirm: false,
            confirmButtonText: "Продолжить",
            allowOutsideClick: false,
            allowEscapeKey: false

        }).then((result) => {
            if (result.value) {

                backToLvlSelect();

            }

        });

        return true;

    }
    else {

        return false;

    }

}

function keyboardFill(usedNumbers) {

    for (let i = 1; i < 10; i++) {

        if (usedNumbers.indexOf(i) === -1)
            $("button[num=" + i + "]").html(i);
        else
            $("button[num=" + i + "]").html("");

    }

    return "keyboard filled";

}

function restartTheGame() {

    initGame();
    echoTable();
    fillTableFromTemplate();

}