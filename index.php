<?php
namespace sudoku;

error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors', 1);

include $_SERVER['DOCUMENT_ROOT'] . "/vendor/autoload.php";
include $_SERVER['DOCUMENT_ROOT'] . "/app/bootstrap.php";